// author: Von Russel Aranillo Oseña


// main app
angular.module('app', ['ui.bootstrap', 'signup']);


angular.module('signup', [])


	.controller('CustomerSignupController', ['$scope', function($scope) {

		$scope.user = {};
		$scope.nameTitleOptions = [
			{id: 'mr', name: 'Mr.'},
			{id: 'ms', name: 'Ms.'},
			{id: 'mrs', name: 'Mrs.'},
			{id: 'dr', name: 'Dr.'}
		]

		$scope.birthdateIsValid = function() {
			var birthdate = $scope.user.dob;
			if(birthdate && $scope.user.dob_check && (getYearsDiff(new Date(), birthdate) < 18)) {
				// check if selectbox for birthdate is valid
				return false;
			}
			return true;
		};

		$scope.submit = function() {
			if($scope.signupForm.$valid) {
				// form is valid on client
				alert('Form is successfully validated!');
			}
		};



	}]);

function getYearsDiff(date1, date2) { // birthday is a date
   var ageDifMs = date1 - date2.getTime();
   var ageDate = new Date(ageDifMs); // miliseconds from epoch
   return Math.abs(ageDate.getUTCFullYear() - 1970);
}